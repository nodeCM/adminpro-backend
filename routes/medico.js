// Libraries
var express = require('express')
// Token
var mdAutentificacion = require('../middlewares/autentificacion');

var app = express()

// Models
var Medico = require('../models/medico')

/*===============================================================================
Obtener los medicos
===============================================================================*/
app.get('/', (req, res) => {

    var desde = req.query.desde || 0;
    desde = Number(desde)

    Medico.find({})
        .skip(desde)
        .limit(5)
        .populate('user', 'name email')
        .populate('hospital')
        .exec((err, medicos) => {

            if (err) {
                return res.status(500).json({
                    ok: false,
                    message: 'Error al obtener medicos',
                    error: err
                })
            }

            Medico.count({}, (err, conteo) => {
                res.status(400).json({
                    ok: true,
                    medicos: medicos,
                    total: conteo
                })
            })
        })
})

/*===============================================================================
Guardar medico
===============================================================================*/
app.post('/', mdAutentificacion.verificaToken, (req, res) => {
    body = req.body;

    var medico = new Medico({
        name: body.name,
        img: body.img,
        user: body.user,
        hospital: body.hospital
    })

    medico.save((err, medicoGuardado) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                message: 'Error al guardar medico',
                error: err
            })
        }
        res.status(200).json({
            ok: true,
            medico: medicoGuardado
        })
    })
})

/*===============================================================================
Actualizar meditco
===============================================================================*/
app.put('/:id', mdAutentificacion.verificaToken, (req, res) => {
    var id = req.params.id
    var body = req.body

    Medico.findById(id, (err, medico) => {

        if (err) return res.status(500).json({ ok: false, message: 'Error al buscar medico', error: err })

        if (!medico) return res.status(400).json({ ok: false, message: 'Error al buscar medico', error: err })

        medico.name = body.name
        medico.img = body.img
        medico.user = body.user
        medico.hospital = body.hospital

        medico.save((err, medicoGuardado) => {
            if (err) return res.status(500).json({ ok: false, message: 'Error al guardar medico', error: err })
            res.status(200).json({
                ok: true,
                medico: medicoGuardado
            })
        })
    })
})

/*===============================================================================
Eliminar medico
===============================================================================*/
app.delete('/:id', mdAutentificacion.verificaToken, (req, res) => {
    var id = req.params.id

    Medico.findByIdAndRemove(id, (err, medico) => {
        if (err) return res.status(500).json({ ok: false, message: 'Error al eliminar medico', error: err })
        if (!medico) return res.status(400).json({ ok: false, message: 'Error al eliminar medico', error: err })
        res.status(200).json({
            ok: true,
            medico: medico
        })
    })
})

module.exports = app;