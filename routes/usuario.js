// Libraries
var express = require('express');
var bcrypt = require('bcryptjs');

var mdAutentificacion = require('../middlewares/autentificacion');

// Variable instanciation
var app = express();

// Models
var Usuario = require('../models/usuario')

/*===============================================================================
Obtener usuarios
===============================================================================*/
app.get('/', (req, res, next) => {

    var desde = req.query.desde || 0;
    desde = Number(desde)

    Usuario.find({}, 'name email img role')
        .skip(desde)
        .limit(5)
        .exec(
            (err, usuarios) => {
                if (err) {
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error cargando usuarios',
                        errors: err
                    })
                }
                Usuario.count({}, (err, conteo) => {
                    res.status(200).json({
                        ok: true,
                        usuarios: usuarios,
                        total: conteo
                    })
                })
            })
})

/*===============================================================================
Crear un nuevo usuario
===============================================================================*/
app.post('/', mdAutentificacion.verificaToken, (req, res) => {
    var body = req.body;

    var usuario = new Usuario({
        name: body.name,
        email: body.email,
        password: bcrypt.hashSync(body.password, 10),
        img: body.img,
        role: body.role
    })

    usuario.save((err, usuarioGuardado) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Error guardando usuario',
                errors: err
            })
        }

        res.status(200).json({
            ok: true,
            body: usuarioGuardado,
            usuarioToken: req.usuario
        })
    })
})

/*===============================================================================
Actualizar usuario
===============================================================================*/
app.put('/:id', mdAutentificacion.verificaToken, (req, res) => {
    var id = req.params.id;
    var body = req.body;

    Usuario.findById(id, (err, usuario) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error consultando usuario',
                errors: err
            })
        }
        if (!usuario) {
            return res.status(4000).json({
                ok: false,
                mensaje: `El usuario con el id ${id} no existe`,
                error: { message: 'No existe usuario con id enviado' }
            })
        }

        usuario.name = body.name;
        usuario.email = body.email;
        usuario.role = body.role

        usuario.save((err, usuarioActualizado) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Error al actualizar usuario',
                    errors: err
                })
            }

            res.status(200).json({
                ok: true,
                body: usuarioActualizado
            })
        })

    })
})

/*===============================================================================
Borrar usuario por id
===============================================================================*/
app.delete('/:id', mdAutentificacion.verificaToken, (req, res) => {
    var id = req.params.id;

    Usuario.findByIdAndRemove(id, (err, usuarioBorrado) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error eliminando usuario',
                errors: err
            })
        }
        if (!usuarioBorrado) {
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe usaurio con eses id',
                errors: { message: 'No existe usaurio con eses id' }
            })
        }
        res.status(200).json({
            ok: true,
            body: usuarioBorrado
        })
    })
})

module.exports = app;