// Libraries
var express = require('express')
var bcrypt = require('bcryptjs')
var jwt = require('jsonwebtoken')

var app = express();

var SEED = require('../config/config').SEED

// Models
var Usaurio = require('../models/usuario')

app.post('/', (req, res) => {

    var body = req.body

    // Verificar si existe un usuario con correo enviado
    Usaurio.findOne({ email: body.email }, (err, usuarioDB) => {

        if (err) {
            res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar usuarios',
                errors: err
            })
        }

        if (!usuarioDB) {
            res.status(400).json({
                ok: false,
                mensaje: 'Credenciales incorrectas - email'
            })
        }

        // Comparar contraseña enviada
        if (!bcrypt.compareSync(body.password, usuarioDB.password)) {
            res.status(400).json({
                ok: false,
                mensaje: 'Credenciales incorrectas - password'
            })
        }

        usuarioDB.password = ': )'
        // Crear un token
        var token = jwt.sign({ usuario: usuarioDB }, SEED, { expiresIn: 14400 }) // 4 horas


        res.status(200).json({
            ok: true,
            usuario: usuarioDB,
            token: token,
            id: usuarioDB.id
        })
    })

})

module.exports = app;
