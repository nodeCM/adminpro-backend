// Libraries
var express = require('express')
// Token
var mdAutentificacion = require('../middlewares/autentificacion');

var app = express();

// Models
var Hospital = require('../models/hospital')

/*===============================================================================
Obtener todos los hospitales
===============================================================================*/
app.get('/', (req, res) => {

    var desde = req.query.desde || 0;
    desde = Number(desde)

    Hospital.find({})
        .skip(desde)
        .limit(5)
        .populate('user', 'name email')
        .exec(
            (err, hospitales) => {
                if (err) {
                    return res.status(500).json({
                        ok: false,
                        mensaje: 'Error cargando hospitales',
                        errors: err
                    })
                }
                Hospital.count({}, (err, conteo) => {
                    res.status(200).json({
                        ok: true,
                        usuarios: hospitales,
                        total: conteo
                    })
                })
            })

})

/*===============================================================================
Guardar un hospital
===============================================================================*/
app.post('/', mdAutentificacion.verificaToken, (req, res) => {

    let body = req.body

    var hospital = new Hospital({
        name: body.name,
        img: body.img,
        user: body.user
    })

    hospital.save((err, hospitalGuardado) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Error guardando hospital',
                errors: err
            })
        }

        res.status(200).json({
            ok: true,
            body: hospitalGuardado
        })
    })
})

/*===============================================================================
Actualizar un hospital
===============================================================================*/
app.put('/:id', mdAutentificacion.verificaToken, (req, res) => {
    var id = req.params.id
    var body = req.body;

    Hospital.findById(id, (err, hospital) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                message: 'Error al buscar hospital',
                error: err
            })
        }
        if (!hospital) {
            return res.status(400).json({
                ok: false,
                message: 'Error al buscar hospital'
            })
        }

        hospital.name = body.name
        hospital.img = body.img
        hospital.user = body.user

        hospital.save((err, hospitalActualizado) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    message: 'Error al actualizar hospital',
                    error: err
                })
            }

            res.status(200).json({
                ok: true,
                hospital: hospitalActualizado
            })
        })
    })
})

/*===============================================================================
Eliminar hospital
===============================================================================*/
app.delete('/:id', mdAutentificacion.verificaToken, (req, res) => {
    let id = req.params.id;

    Hospital.findByIdAndRemove(id, (err, hospitalEliminado) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                message: 'Error al eliminar hospital',
                error: err
            })
        }
        if (!hospitalEliminado) {
            return res.status(400).json({
                ok: false,
                message: 'Error al eliminar hospital'
            })
        }
        res.status(200).json({
            ok: true,
            hospital: hospitalEliminado
        })
    })

})

module.exports = app