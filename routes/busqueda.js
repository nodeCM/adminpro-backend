var express = require('express');

var app = express();

var Hospital = require('../models/hospital')
var Medico = require('../models/medico')

app.get('/todo/:busqueda', (req, res, next) => {
    var busqueda = req.params.busqueda;
    var regex = new RegExp(busqueda, 'i')

    buscarHospitales(busqueda, regex).then(hospitales => {
        res.status(200).json({
            ok: true,
            hospitales: hospitales
        })
    })
})

function buscarHospitales(busqueda, regex) {

    return new Promise((resolve, reject) => {

        Hospital.find({ name: regex }, (err, hospitales) => {
            if (err) {
                reject('Error al cargar hospitales' + err)
            }else{
                resolve(hospitales)
            }
        })

    })
}

function buscarMedicos(busqueda, regex) {

    return new Promise((resolve, reject) => {

        Medico.find({ name: regex }, (err, hospitales) => {
            if (err) {
                reject('Error al cargar medicos' + err)
            }else{
                resolve(hospitales)
            }
        })

    })
}

module.exports = app;